
import glob
import pickle

files = []
for fileName in glob.glob('logs/*.dump'):
    files.append(pickle.load(open(fileName, 'rb')))

toSave = files.pop(0)

for data in files:
    for key, value in data.items():
        toSave[key] = value

pickle.dump(toSave, open('merged.dump', 'wb'))
