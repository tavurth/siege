#! /usr/bin/python

import os
import sys
import time
import pickle
import getopt
import signal
import subprocess
# import numpy as np
# import matplotlib as plt

GLOBAL = {
    'FLAGS': {
        'TEST': False
    },

    'MAX_USERS' : 200,
    'MIN_USERS' : 20,
    'INC_USERS' : 20,
    'INC_LOG'   : 1.1,
    'SERVERS'   : [],
    'TIME'      : 20,
    'REPORT'    : {},
}

try:
    os.mkdir('./logs', 0755)

except Exception as e:
    pass

def usage():
    print 'Help menu:'
    print '\t', '-s --server=your.domain.com'
    print '\t', '-a --minUsers=starting load'
    print '\t', '-r --maxUsers=finishing load'
    print '\t', '-i --incUsers=Incremental load'
    print '\t', '-l --incLog=Logarithmic incremental load'
    print '\t', '-t --time=Number of seconds to run each test'
    print '\t', '-h --help Display this help menu'

def test_server(serverName):

    global GLOBAL
    currUsers = GLOBAL['MIN_USERS']

    currReport = {}

    while currUsers < GLOBAL['MAX_USERS']:
        stream = None
        try:

            print '-' * 140
            print '\tTimeout in', GLOBAL['TIME']
            print '\tUsing', currUsers, 'clients threads'
            print '\tTesting server', serverName

            logPart = serverName
            if '//' in logPart:
                logPart = logPart[serverName.find('//') + 2 :]

            logName = 'logs/' + logPart + '_' + str(currUsers) + '.txt'
            string = 'siege '+ serverName +' -q -c '+ str(currUsers) +' --internet --time="'+ str(GLOBAL['TIME']) +'s" --log="' + logName + '"'

            print '\r\n\tRunning as:\r\n\t\t', string
            print '-' * 140

            stream = subprocess.Popen(string, shell=True, stdout=subprocess.PIPE)

            time.sleep(GLOBAL['TIME'] + 1)

            try:

                # Read the lines into memory again
                # Could be cleaner
                with open(logName, 'r') as fin:
                    lines = fin.readlines()

                    # Saving report
                    if len(lines) > 1:

                        # Extracting the parts
                        lineA = [ line.strip() for line in lines[0].split(',') ]
                        lineB = [ line.strip() for line in lines[1].split(',') ]

                        # Combining the parts
                        report = {}
                        for i in range(len(lineA)):
                            report[lineA[i]] = lineB[i]

                        # Saving the report in memory
                        currReport[str(currUsers)] = report

                # Remove file
                os.remove(logName)

            except Exception as e:
                print 'Failed with', e
                pass

            currUsers = int(currUsers * GLOBAL['INC_LOGN']) + GLOBAL['INC_USERS']

        except KeyboardInterrupt as e:
            exit(0);

    return currReport

def test_servers():

    global GLOBAL

    for serverName in GLOBAL['SERVERS']:
        pickle.dump(test_server(serverName), open('logs/'+ serverName +'.dump', "wb"))

def collect_args(*args):
    global GLOBAL

    # Try to extract the arguments
    try:
        opts, args = getopt.getopt(args, "s:a:b:i:l:t:h", ["server=", "minUsers=", "maxUsers=", "incUsers=", "incLog=", "time=", 'help'])

    # Exception while grabbing arguments
    except getopt.GetoptError as err:
        print err
        usage()
        sys.exit(2)

    # Parse each of the input options
    for opt, arg in opts:

        # Server to test
        if opt in ("-s", "--server"):
            GLOBAL['SERVERS'].append(arg)

        # Starting number of users (load)
        elif opt in ("-a", "--minUsers"):
            GLOBAL['MIN_USERS'] = int(arg)

        # Max number of users to test with
        elif opt in ("-b", "--maxUsers"):
            GLOBAL['MAX_USERS'] = int(arg)

        # User increment integer
        elif opt in ("-i", "--incUsers"):
            GLOBAL['INC_USERS'] = int(arg)

        # User increment logarithmic float
        elif opt in ("-l", "--incLog"):
            GLOBAL['INC_LOGN'] = float(arg)

        # Time to run each test
        elif opt in ("-t", "--time"):
            GLOBAL['TIME'] = int(arg)

        # Print the help dialog
        elif opt in ("-h", "--help"):
            usage()
            exit()

if __name__ == "__main__":

    # Collect system arguments
    collect_args(*sys.argv[1:])

    # Make sure we have some files to parse
    if not len(GLOBAL['SERVERS']):
        usage()
        exit(0)

    test_servers()
