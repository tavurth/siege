
import pickle
import numpy as np
from prettyprint import pp
import matplotlib.pyplot as plt

data = pickle.load(open('merged.dump', 'rb'))

rate = []
for key, val in data.items():

    load = int(key)
    ok = float(val['OKAY'])
    no = float(val['Failed'])
    rate.append([load, (1 - no / (ok+no)) * 100, ok, no])

rate = np.asarray(rate)
rate = rate[rate[:,1].argsort()]

plt.plot(rate[:,0], rate[:,1], 'bo', rate[:,0], rate[:,1], 'k')

plt.title('Stress test: https://au.finedefender.com', fontsize=14, color='blue')
plt.ylabel('% Avilibility of server resources', fontsize=14, color='blue')
plt.xlabel('Number of concurrant users', fontsize=14, color='blue')
plt.show()
